function counterFactory() {
    let c = 5
    function increment()
    {
      c += 1;
      return c;
    }
    function decrement()
    {
      c -= 1;
      return c;
    }
    return {
      increment,
      decrement
    }
  }
module.exports=counterFactory;