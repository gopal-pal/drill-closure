const cacheFunction=require("../cacheFunction");

const cb=(x)=>x*2; 
const result = cacheFunction(cb);

console.log(result(5));
console.log(result(6));
result(5);