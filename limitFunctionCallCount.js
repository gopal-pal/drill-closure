function limitFunctionCallCount(cb, n) 
{
  let c = 0;
  function call()
  {
    if (c < n)
    {
      c += 1;
      cb();
    }
    else
      console.log('Callback already called ' + n + '-times')
  }
  return {call};
}
module.exports = limitFunctionCallCount;